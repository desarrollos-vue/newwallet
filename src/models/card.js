const newCard = ( card = {

    id : null,
    CardNumber : '',
    CardName : '',
    CardExpMonth : '',
    CardExpYear : '',
    CardCode : null,
    defaultCard:false
  
  }) => {
  
    return {
  
      id : card.id,
      CardName : card.CardName,
      CardNumber : card.CardNumber,
      CardExpMonth : card.CardExpMonth,
      CardExpYear : card.CardExpYear,
      CardCode : card.CardCode,
      defaultCard:false  
    };
  
  };
  
  export default  {
  
    newCard
  
  }